package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class DiemDon {
    @SerializedName("id")
    private int id;
    @SerializedName("noidi")
    private String noidi;
    @SerializedName("diemdon")
    private String diemdon;
    @SerializedName("ghichu")
    private String ghichu;

    public int getId() {
        return id;
    }

    public String getNoidi() {
        return noidi;
    }

    public String getDiemdon() {
        return diemdon;
    }

    public String getGhichu() {
        return ghichu;
    }
}
