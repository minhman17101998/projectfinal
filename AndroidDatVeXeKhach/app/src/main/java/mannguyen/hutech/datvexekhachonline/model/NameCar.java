package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NameCar {

    private List<DBean> d;

    public List<DBean> getD() {
        return d;
    }

    public void setD(List<DBean> d) {
        this.d = d;
    }

    public static class DBean {
        @SerializedName("nhaxe")
        private String nhaxe;

        public String getNhaxe() {
            return nhaxe;
        }

        public void setNhaxe(String nhaxe) {
            this.nhaxe = nhaxe;
        }
    }
}
