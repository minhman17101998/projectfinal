package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class InforCus {
    @SerializedName("tenkhachhang")
    private String tenkhachhang;
    @SerializedName("ngaysinh")
    private String ngaysinh;
    @SerializedName("sdt")
    private String sdt;
    @SerializedName("diachi")
    private String diachi;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("gioitinh")
    private String gioitinh;
    @SerializedName("id_user")
    private int id_user;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTenkhachhang(String tenkhachhang) {
        this.tenkhachhang = tenkhachhang;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }



    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public int getId_user() {
        return id_user;
    }

    public String getTenkhachhang() {
        return tenkhachhang;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getDiachi() {
        return diachi;
    }



}
