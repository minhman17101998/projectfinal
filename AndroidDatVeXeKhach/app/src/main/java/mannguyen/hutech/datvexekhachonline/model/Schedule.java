package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("machuyen")
    private int machuyen;
    @SerializedName("diemdi")
    private String diemdi;
    @SerializedName("diemden")
    private String diemden;
    @SerializedName("ngaydi")
    private String ngaydi;
    @SerializedName("giodi")
    private String giodi;
    @SerializedName("nhaxe")
    private String nhaxe;
    @SerializedName("loaixe")
    private String loaixe;
    @SerializedName("giave")
    private int giave;
    @SerializedName("ghichu")
    private String ghichu;

    public int getMachuyen() {
        return machuyen;
    }

    public String getDiemdi() {
        return diemdi;
    }


    public String getDiemden() {
        return diemden;
    }

    public String getNgaydi() {
        return ngaydi;
    }

    public String getGiodi() {
        return giodi;
    }

    public String getNhaxe() {
        return nhaxe;
    }

    public String getLoaixe() {
        return loaixe;
    }

    public int getGiave() {
        return giave;
    }

    public String getGhichu() {
        return ghichu;
    }


}
