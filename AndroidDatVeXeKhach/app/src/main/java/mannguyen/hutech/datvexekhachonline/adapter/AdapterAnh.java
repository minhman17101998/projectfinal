package mannguyen.hutech.datvexekhachonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.model.Anh;

public class AdapterAnh extends BaseAdapter {
    Activity context;
    ArrayList<Anh> list;

    public AdapterAnh(Activity context, ArrayList<Anh> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.danh_sach_ve,null);

        ImageView img_anh = row.findViewById(R.id.img_anh);
        TextView txt_nhaxe = row.findViewById(R.id.txt_nhaxe);
        TextView txt_diemdi = row.findViewById(R.id.txt_diemdi);
        TextView txt_diemden = row.findViewById(R.id.txt_diemden);
        TextView txt_ngaydi = row.findViewById(R.id.txt_ngaydi);
        TextView txt_giodi = row.findViewById(R.id.txt_giodi);
        TextView txt_soghe = row.findViewById(R.id.txt_soghe);

        Anh anh1111 = list.get(position);

        txt_nhaxe.setText(anh1111.getTennhaxe());
        txt_diemdi.setText(anh1111.getDiemdi());
        txt_diemden.setText(anh1111.getDiemden());
        txt_ngaydi.setText(anh1111.getNgaydi());
        txt_giodi.setText(anh1111.getGiodi());
        txt_soghe.setText(anh1111.getSoghe());

        Bitmap anhaa = BitmapFactory.decodeByteArray(anh1111.getAnh(),0,anh1111.getAnh().length);
        img_anh.setImageBitmap(anhaa);

        return row;
    }
}
