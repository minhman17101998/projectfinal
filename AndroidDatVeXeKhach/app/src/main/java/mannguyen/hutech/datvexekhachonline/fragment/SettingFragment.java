package mannguyen.hutech.datvexekhachonline.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.UserID;
import mannguyen.hutech.datvexekhachonline.login.InforUserActivity;
import mannguyen.hutech.datvexekhachonline.login.LoginActivity;
import mannguyen.hutech.datvexekhachonline.model.InforCus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment {
    Calendar cal;
    Date dateFinish;
    String id;
    TextView txtTenKH, txtThongTinKH, txtDangXuat;
    View mView;
    Spinner spnGioiTinh;
    EditText edt_sdt, edtTenKH_thong_tin, edtNgaySinh, edtDiaChiKH;
    Button btnChonNgaySinh, btnSua;

    public SettingFragment(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_setting, container, false);

        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
        txtTenKH = root.findViewById(R.id.txtTenKH);
        txtThongTinKH = root.findViewById(R.id.txtThongTinKH);
        txtDangXuat = root.findViewById(R.id.txtDangXuat);

        txtThongTinKH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suaThongTin();
            }
        });
        return root;
    }

    public void suaThongTin() {
        AlertDialog.Builder mbuilder = new AlertDialog.Builder(getActivity());
        mView = getLayoutInflater().inflate(R.layout.dialod_thong_tin_kh, null);
        spnGioiTinh = mView.findViewById(R.id.spnGioiTinh);
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add(0, "Nam");
        arrayList.add(1, "Nữ");
        ArrayAdapter arrayAdapter1 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnGioiTinh.setAdapter(arrayAdapter1);

        edt_sdt = mView.findViewById(R.id.edt_sdt);
        edtTenKH_thong_tin = mView.findViewById(R.id.edtTenKH_thong_tin);
        edtDiaChiKH = mView.findViewById(R.id.edtDiaChiKH);
        edtNgaySinh = mView.findViewById(R.id.edtNgaySinh);
        btnChonNgaySinh = mView.findViewById(R.id.btnChonNgaySinh);
        btnSua = mView.findViewById(R.id.btnSua);

        getDefaultInfor();

        btnChonNgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialogNgayDi();
            }
        });
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<InforCus>> call = datVeXe.getInforUser(id);
        call.enqueue(new Callback<List<InforCus>>() {
            @Override
            public void onResponse(Call<List<InforCus>> call, Response<List<InforCus>> response) {
                if (response.isSuccessful() && response != null) {
                    List<InforCus> inforCuses = response.body();
                    for (int i = 0; i < inforCuses.size(); i++) {
                        edtTenKH_thong_tin.setText(inforCuses.get(i).getTenkhachhang());
                        edt_sdt.setText(inforCuses.get(i).getSdt());
                        edtNgaySinh.setText(inforCuses.get(i).getNgaysinh());
                        edtDiaChiKH.setText(inforCuses.get(i).getDiachi());
                        String gioitinh = inforCuses.get(i).getGioitinh();
                        if (gioitinh.equals("Nam")) {
                            spnGioiTinh.setSelection(0);
                        } else {
                            spnGioiTinh.setSelection(1);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InforCus>> call, Throwable t) {

            }
        });
        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Update();
            }
        });
        mbuilder.setView(mView);

        AlertDialog dialog1 = mbuilder.create();
        dialog1.setTitle("SỬA THÔNG TIN");
        dialog1.show();
    }

    public void getDefaultInfor() {
        //lấy ngày hiện tại của hệ thống
        cal = Calendar.getInstance();
        SimpleDateFormat dft = null;
        //Định dạng ngày / tháng /năm
        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(cal.getTime());
        //hiển thị lên giao diện
        edtNgaySinh.setText(strDate);
    }

    public void showDatePickerDialogNgayDi() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                //Mỗi lần thay đổi ngày tháng năm thì cập nhật lại TextView Date
                String day, month1;
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else
                    day = String.valueOf(dayOfMonth);
                if (monthOfYear < 9) {
                    month1 = "0" + (monthOfYear+1);
                } else {
                    month1 = String.valueOf(monthOfYear+1);
                }
                edtNgaySinh.setText(
                        (day) + "/" + (month1) + "/" + year);

                cal.set(year, monthOfYear, dayOfMonth);
                dateFinish = cal.getTime();
            }
        };
        //các lệnh dưới này xử lý ngày giờ trong DatePickerDialog
        //sẽ giống với trên TextView khi mở nó lên
        String s = edtNgaySinh.getText() + "";
        String strArrtmp[] = s.split("/");
        int ngay = Integer.parseInt(strArrtmp[0]);
        int thang = Integer.parseInt(strArrtmp[1]) - 1;
        int nam = Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                getActivity(),
                callback, nam, thang, ngay);
        pic.setTitle("CHỌN NGÀY SINH");
        pic.show();
    }

    public void Update()
    {
        String ten = edtTenKH_thong_tin.getText().toString().trim();
        String ns = edtNgaySinh.getText().toString().trim();
        String sdt = edt_sdt.getText().toString().trim();
        String gt = spnGioiTinh.getSelectedItem().toString().trim();
        String dc = edtDiaChiKH.getText().toString().trim();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<InforCus> call = datVeXe.updateinfor(ten,ns,gt,sdt,dc, Integer.parseInt(id));
        call.enqueue(new Callback<InforCus>() {
            @Override
            public void onResponse(Call<InforCus> call, Response<InforCus> response) {
                if (response.isSuccessful()&&response!=null)
                {
                    Boolean success = response.body().isSuccess();
                    if (success)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                        b.setTitle("Thông báo");
                        b.setMessage("Cập nhật thông tin thành công");
                        b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }else
                    {
                        Toast.makeText(getActivity(), "Lỗi", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<InforCus> call, Throwable t) {

            }
        });
    }
}
