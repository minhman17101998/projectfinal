package mannguyen.hutech.datvexekhachonline.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.Database.Database;
import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.model.InforCus;
import mannguyen.hutech.datvexekhachonline.model.users;
import mannguyen.hutech.datvexekhachonline.view.CustomorInforActivity;
import mannguyen.hutech.datvexekhachonline.view.MainActivity;
import mannguyen.hutech.datvexekhachonline.view.SaveQRCodeActivity;
import mannguyen.hutech.datvexekhachonline.view.ThanhToanActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InforUserActivity extends AppCompatActivity {
    Calendar cal;
    Date dateFinish;
    final String DATABASE_NAME = "datvexe.sqlite";
    TextView txtfont2;
    private EditText edtHoTen, edtSDT, edtSoNha,edtNgaySinh;
    private Spinner spnTinh, spnHuyen, spnXa,spnGioiTinh;
    private Button btn_OK,btnChonNgay;
    public  String tendangnhap,id,ngaysinh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infor_user);

        txtfont2 = findViewById(R.id.txtfont2);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/font.TTF");

        txtfont2.setTypeface(typeface);

        AnhXa();
        getDefaultInfor();

        btnChonNgay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialogNgayDi();
            }
        });

        Intent intent = getIntent();
        tendangnhap = intent.getStringExtra("tdn");


       addControls();
    }

    private void AnhXa() {

        edtHoTen = findViewById(R.id.edt_Name_customer);
        edtSDT = findViewById(R.id.edt_Phone_customer);
        edtSoNha = findViewById(R.id.edtSoNha);
        edtNgaySinh = findViewById(R.id.edt_ngaysinh);
        spnTinh = findViewById(R.id.spnTinh);
        spnHuyen = findViewById(R.id.spnHuyen);
        spnXa = findViewById(R.id.spnXa);

        btn_OK = findViewById(R.id.btn_OK);
        btnChonNgay = findViewById(R.id.btnChonNgay);
        spnGioiTinh = findViewById(R.id.spn_gioitinh);

    }
    public void getTinh() {
        ArrayList<String> arrTinh = new ArrayList<String>();
        final SQLiteDatabase database = Database.initDatabase(this, DATABASE_NAME);
        Cursor cursor = database.rawQuery("SELECT * FROM tinh", null);
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String name = cursor.getString(1);
            arrTinh.add(name);
        }
        Collections.sort(arrTinh);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrTinh);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTinh.setAdapter(arrayAdapter);
    }

    public void getHuyen(String ma) {
        ArrayList<String> arrHuyen = new ArrayList<String>();
        SQLiteDatabase database = Database.initDatabase(this, DATABASE_NAME);
        Cursor cursor2 = database.rawQuery("SELECT * FROM huyen where tinh_id = ?", new String[]{ma});
        for (int i = 0; i < cursor2.getCount(); i++) {
            cursor2.moveToPosition(i);
            String name = cursor2.getString(1);
            arrHuyen.add(name);
        }
        Collections.sort(arrHuyen);
        ArrayAdapter arrayAdapter = new ArrayAdapter(InforUserActivity.this, android.R.layout.simple_spinner_item, arrHuyen);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnHuyen.setAdapter(arrayAdapter);
    }

    private void getXa(String mahuyen) {
        ArrayList<String> arrXa = new ArrayList<String>();
        SQLiteDatabase database = Database.initDatabase(this, DATABASE_NAME);
        Cursor cursor2 = database.rawQuery("SELECT * FROM xa where huyen_id = ?", new String[]{mahuyen});
        for (int i = 0; i < cursor2.getCount(); i++) {
            cursor2.moveToPosition(i);
            String name = cursor2.getString(1);
            arrXa.add(name);
        }
        Collections.sort(arrXa);
        ArrayAdapter arrayAdapter = new ArrayAdapter(InforUserActivity.this, android.R.layout.simple_spinner_item, arrXa);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnXa.setAdapter(arrayAdapter);
    }
    public void getDefaultInfor() {
        //lấy ngày hiện tại của hệ thống
        cal = Calendar.getInstance();
        SimpleDateFormat dft = null;
        //Định dạng ngày / tháng /năm
        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(cal.getTime());
        //hiển thị lên giao diện
        edtNgaySinh.setText(strDate);
    }
    public void addControls() {
        getTinh();
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add(0,"Nam");
        arrayList.add(1,"Nữ");
        ArrayAdapter arrayAdapter1= new ArrayAdapter(this,android.R.layout.simple_spinner_item,arrayList);
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnGioiTinh.setAdapter(arrayAdapter1);

        spnTinh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object tentinh = parent.getItemAtPosition(position);
                final SQLiteDatabase database = Database.initDatabase(InforUserActivity.this, DATABASE_NAME);
                Cursor cursor1 = database.rawQuery("SELECT * FROM tinh where name = ?", new String[]{String.valueOf(tentinh)});
                cursor1.moveToFirst();
                final String matinh = cursor1.getString(0);
                getHuyen(matinh);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnHuyen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object tenhuyen = parent.getItemAtPosition(position);
                SQLiteDatabase database = Database.initDatabase(InforUserActivity.this, DATABASE_NAME);
                Cursor cursor = database.rawQuery("SELECT * FROM huyen where name = ?", new String[]{String.valueOf(tenhuyen)});
                cursor.moveToFirst();
                String mahuyen = cursor.getString(0);
                getXa(mahuyen);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_OK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String hoten = edtHoTen.getText().toString();
                        String sdt = edtSDT.getText().toString();


                        if (hoten.equals("")) {
                            edtHoTen.setError("Nhập họ tên");
                        } else if (sdt.equals("")) {
                            edtSDT.setError("Nhập số điện thoại");
                        } else {
                            getID();
                            Intent intent = new Intent(InforUserActivity.this, LoginActivity.class);
                            intent.putExtra("id_user",id);
                            startActivity(intent);
                        }
                    }
                });
            }
        });


    }

    private void getID(){
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<users>> call = datVeXe.getId(tendangnhap);
        call.enqueue(new Callback<List<users>>() {
            @Override
            public void onResponse(Call<List<users>> call, Response<List<users>> response) {
                if (response.isSuccessful())
                {
                    List<users> userss = response.body();
                    for (int i = 0 ; i< userss.size();i++)
                    {
                        id = userss.get(i).getId();
                        //Toast.makeText(InforUserActivity.this, id, Toast.LENGTH_SHORT).show();
                        addKhachHang(Integer.parseInt(id));
                    }

                }
                else
                {
                    Log.i("Thong Bao:","Loi");
                }
            }

            @Override
            public void onFailure(Call<List<users>> call, Throwable t) {

            }
        });

    }
    private void addKhachHang(int matk)
    {
        String ten = edtHoTen.getText().toString();
        String gioitinh = spnGioiTinh.getSelectedItem().toString();
        String sodienthoai = edtSDT.getText().toString();
        String diachi = edtSoNha.getText()+", "
                +spnXa.getSelectedItem().toString()+", "
                +spnHuyen.getSelectedItem().toString()+", "
                +spnTinh.getSelectedItem().toString();
        String ns = edtNgaySinh.getText().toString();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<InforCus> call = datVeXe.getInfor(ten,ns,gioitinh,sodienthoai,diachi, matk);
        call.enqueue(new Callback<InforCus>() {
            @Override
            public void onResponse(Call<InforCus> call, Response<InforCus> response) {
                if(response.isSuccessful() && response.body() != null){
                    Boolean success =  response.body().isSuccess();
                    if (success){
                        Log.i("Thong bao:","Them infor Thanh cong");
                    }else{
                        Toast.makeText(InforUserActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    }}
            }



            @Override
            public void onFailure(Call<InforCus> call, Throwable t) {

            }
        });
    }


    public void showDatePickerDialogNgayDi()
    {
        DatePickerDialog.OnDateSetListener callback=new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                //Mỗi lần thay đổi ngày tháng năm thì cập nhật lại TextView Date
                String day, month1;
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else
                    day = String.valueOf(dayOfMonth);
                if (monthOfYear < 10) {
                    month1 = "0" + (monthOfYear + 1);
                } else {
                    month1 = String.valueOf(monthOfYear);
                }
                edtNgaySinh.setText(
                        (day) +"/"+(month1)+"/"+year);
                ngaysinh = year+"-"+month1+"-"+day;
                cal.set(year, monthOfYear, dayOfMonth);
                dateFinish=cal.getTime();
            }
        };
        //các lệnh dưới này xử lý ngày giờ trong DatePickerDialog
        //sẽ giống với trên TextView khi mở nó lên
        String s = edtNgaySinh.getText()+"";
        String strArrtmp[]=s.split("/");
        int ngay=Integer.parseInt(strArrtmp[0]);
        int thang=Integer.parseInt(strArrtmp[1])-1;
        int nam=Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                InforUserActivity.this,
                callback, nam, thang, ngay);
        pic.setTitle("CHỌN NGÀY SINH");
        pic.show();


    }
}
