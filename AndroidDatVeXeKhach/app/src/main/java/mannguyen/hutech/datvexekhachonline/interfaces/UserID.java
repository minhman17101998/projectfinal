package mannguyen.hutech.datvexekhachonline.interfaces;

public class UserID {
    public String id;

    public UserID(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
