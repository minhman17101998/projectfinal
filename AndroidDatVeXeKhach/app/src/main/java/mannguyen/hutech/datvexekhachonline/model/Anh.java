package mannguyen.hutech.datvexekhachonline.model;

public class Anh {
    private int id;
    private byte[] anh;
    private String tennhaxe;
    private String diemdi;
    private String diemden;
    private String ngaydi;
    private String giodi;
    private String soghe;
    public Anh() {
    }

    public Anh(int id, byte[] anh, String tennhaxe, String diemdi, String diemden, String ngaydi, String giodi, String soghe) {
        this.id = id;
        this.anh = anh;
        this.tennhaxe = tennhaxe;
        this.diemdi = diemdi;
        this.diemden = diemden;
        this.ngaydi = ngaydi;
        this.giodi = giodi;
        this.soghe = soghe;
    }

    public String getTennhaxe() {
        return tennhaxe;
    }

    public void setTennhaxe(String tennhaxe) {
        this.tennhaxe = tennhaxe;
    }

    public String getDiemdi() {
        return diemdi;
    }

    public void setDiemdi(String diemdi) {
        this.diemdi = diemdi;
    }

    public String getDiemden() {
        return diemden;
    }

    public void setDiemden(String diemden) {
        this.diemden = diemden;
    }

    public String getNgaydi() {
        return ngaydi;
    }

    public void setNgaydi(String ngaydi) {
        this.ngaydi = ngaydi;
    }

    public String getGiodi() {
        return giodi;
    }

    public void setGiodi(String giodi) {
        this.giodi = giodi;
    }

    public String getSoghe() {
        return soghe;
    }

    public void setSoghe(String soghe) {
        this.soghe = soghe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getAnh() {
        return anh;
    }

    public void setAnh(byte[] anh) {
        this.anh = anh;
    }
}
