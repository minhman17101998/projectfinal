package mannguyen.hutech.datvexekhachonline.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mannguyen.hutech.datvexekhachonline.R;

public class CustomView extends FrameLayout {
    TextView number_seat;
    ImageView img_seat;
    public CustomView(Context context)
    {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.grid_view_choose_seat, this);
        number_seat = findViewById(R.id.txt_number_chair);
        img_seat = findViewById(R.id.img_chair);
    }

    public void display(String text, boolean isSelected)
    {
        number_seat.setText(text);
        display(isSelected);
    }

    public void display(boolean isSelected)
    {
        img_seat.setImageResource(isSelected? R.drawable.dachon: R.drawable.chuachon);
    }

}
