package mannguyen.hutech.datvexekhachonline.interfaces;

import java.util.List;
import java.util.Map;

import mannguyen.hutech.datvexekhachonline.model.DiemDen;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.model.DiemDon;
import mannguyen.hutech.datvexekhachonline.model.InforCus;
import mannguyen.hutech.datvexekhachonline.model.KhachHang;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import mannguyen.hutech.datvexekhachonline.model.Tickets;
import mannguyen.hutech.datvexekhachonline.model.users;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterfaces {
    @GET("schedule.php")
    Call<List<Schedule>> getSchedules();

    @GET("soghe.php")
    Call<List<KhachHang>> getSoGhe(@Query("machuyen") String machuyen);

    @GET("search.php")
    Call<List<Schedule>> getScheduleforNameCar(@Query("diemdi") String diemdi,
                                               @Query("diemden") String diemden,
                                               @Query("ngaydi") String ngaydi);

    @GET("searchs.php")
    Call<List<Schedule>> getScheduleforNameCars(@Query("ngaydi") String ngaydi);

    @GET("theonhaxe.php")
    Call<List<Schedule>> getSchedule(@Query("nhaxe") String nhaxe,
                                     @Query("diemdi") String diemdi,
                                     @Query("diemden") String diemden,
                                     @Query("ngaydi") String ngaydi);

    @FormUrlEncoded
    @POST("muave.php")
    Call<KhachHang> saveKH(@Field("tenkhachhang") String tenkhachhang,
                           @Field("sodienthoai") String sodienthoai,
                           @Field("diachi") String diachi,
                           @Field("machuyen") int machuyen,
                           @Field("soghe") String soghe,
                           @Field("diemdon") String diemdon);

    @FormUrlEncoded
    @POST("themve.php")
    Call<Tickets> addTicket(@Field("tenve") String tenve,
                            @Field("makhachhang") int makhachhang,
                            @Field("machuyen") int machuyen
                            );
    @GET("makh.php")
    Call<List<KhachHang>> getMaKH(@Query("tenkhachhang") String tenkhachhang,
                            @Query("sodienthoai") String sodienthoai,
                            @Query("diachi") String diachi,
                            @Query("machuyen") int machuyen,
                            @Query("soghe") String soghe);
    @GET("noidi.php")
    Call<List<DiemDi>> getNoiDi();

    @GET("noiden.php")
    Call<List<DiemDen>> getNoiDen(@Query("diemdi") String diemdi);

    @GET("diemdon.php")
    Call<List<DiemDon>> getDiemDon(@Query("noidi") String noidi);
    @FormUrlEncoded
    @POST("login.php")
    Call<users> getLogin(@Field("username") String username,
                         @Field("password") String password);
    @FormUrlEncoded
    @POST("dangky.php")
    Call<users> getDangKy(@Field("username") String username,
                          @Field("password") String password,
                          @Field("email") String email);
    @FormUrlEncoded
    @POST("infor.php")
    Call<InforCus> getInfor(@Field("tenkhachhang") String tenkhachhang,
                            @Field("ngaysinh") String ngaysinh,
                            @Field("gioitinh") String gioitinh,
                            @Field("sdt") String sdt,
                            @Field("diachi") String diachi,
                            @Field("id_user") int id_user);

    @GET("getid.php")
    Call<List<users>> getId(@Query("username") String username);
    @GET("thongtinuser.php")
    Call<List<InforCus>> getInforUser(@Query("id_user") String id_user );
    @FormUrlEncoded
    @POST("updateinfor.php")
    Call<InforCus> updateinfor(@Field("tenkhachhang") String tenkhachhang,
                               @Field("ngaysinh") String ngaysinh,
                               @Field("gioitinh") String gioitinh,
                               @Field("sdt") String sdt,
                               @Field("diachi") String diachi,
                               @Field("id_user") int id_user);

}
