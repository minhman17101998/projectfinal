package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class Tickets {
    @SerializedName("id")
    private int id;
    @SerializedName("tenve")
    private String tenve;
    @SerializedName("makhachhang")
    private int makhachhang;
    @SerializedName("machuyen")
    private int machuyen;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;

    @SerializedName("anh")
    private String anh;


    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenve() {
        return tenve;
    }

    public void setTenve(String tenve) {
        this.tenve = tenve;
    }

    public int getMakhachhang() {
        return makhachhang;
    }

    public void setMakhachhang(int makhachhang) {
        this.makhachhang = makhachhang;
    }

    public int getMachuyen() {
        return machuyen;
    }

    public void setMachuyen(int machuyen) {
        this.machuyen = machuyen;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
