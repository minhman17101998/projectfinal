package mannguyen.hutech.datvexekhachonline.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.EmailValidator;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.users;
import mannguyen.hutech.datvexekhachonline.view.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    TextView txtTenDK,txtDNhap;
    Button btnSignUp;
    EditText edt_email,edt_usernamesign,edt_passwordsign,edt_passwordsigncf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        AnhXa();
        addControls();
    }

    private void addControls() {
        txtDNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dangky();
            }
        });
    }

    private void AnhXa() {
        txtTenDK = findViewById(R.id.txtTenDK);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/font.TTF");
        txtTenDK.setTypeface(typeface);
        txtDNhap = findViewById(R.id.txtDNhap);
        btnSignUp = findViewById(R.id.btnSignUp);
        edt_email = findViewById(R.id.edt_email);
        edt_usernamesign = findViewById(R.id.edt_usernamesign);
        edt_passwordsign = findViewById(R.id.edt_passwordsign);
        edt_passwordsigncf = findViewById(R.id.edt_passwordsigncf);
    }

    private  void dangky()
    {
        String email = edt_email.getText().toString().trim();
        final String username = edt_usernamesign.getText().toString();
        String pass = edt_passwordsign.getText().toString();
        String pass2 = edt_passwordsigncf.getText().toString();

        EmailValidator validator = new EmailValidator();
        if (!validator.validate(email)) {
            edt_email.setError("Email không hợp lệ");
        }
        else if (email.equals(""))
        {
            edt_email.setError("Vui lòng nhập email");
        }else if(username.equals(""))
        {
            edt_usernamesign.setError("Vui lòng nhập tên đăng nhập");
        }else if(username.length()<8)
        {
            edt_usernamesign.setError("Vui lòng nhập tên đăng nhập nhiều hơn 8 kí tự");
        }else if (pass.equals(""))
        {
            edt_passwordsign.setError("Vui lòng nhập mật khẩu");
        }
        else if (pass.length()<6)
        {
            edt_passwordsign.setError("Vui lòng nhập mật khẩu nhiều hơn 6 ký tự");
        }
        else if (pass2.equals(""))
        {
            edt_passwordsigncf.setError("Vui lòng nhập mật khẩu");
        }else if (!pass.equals(pass2))
        {
            edt_passwordsigncf.setError("Mật khẩu nhập lại không chính xác");
        }
        else{
            ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
            Call<users> call = datVeXe.getDangKy(username,pass,email);
            call.enqueue(new Callback<users>() {
                @Override
                public void onResponse(Call<users> call, Response<users> response) {
                    if (response.isSuccessful()&&response.body()!=null){
                        users userss = response.body();
                        if (userss.isSuccess())
                        {
                            Intent intent = new Intent(SignUpActivity.this,InforUserActivity.class);
                            intent.putExtra("tdn",edt_usernamesign.getText().toString());
                            startActivity(intent);
                        }else {
                            edt_usernamesign.setError("Ten dang nhap da ton tai");
                        }
                    }
                }

                @Override
                public void onFailure(Call<users> call, Throwable t) {

                }
            });
        }
    }
}
