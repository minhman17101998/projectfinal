package mannguyen.hutech.datvexekhachonline.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.UserID;
import mannguyen.hutech.datvexekhachonline.model.DiemDen;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.view.ScheduleActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VeFragment extends Fragment {
    Calendar cal;
    TextView txt_today,txtNoiDi;
    Date dateFinish;
    EditText edt_diem_di,edt_diem_den,edt_search,edt_search_diem_den;
    ArrayList<String> aaa =  new ArrayList<String>();
    ArrayList<String> bbb =  new ArrayList<String>();;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter1;
    String noidi;
    ListView lsv_diem_di,lsv_diem_den;
    View mView,mView1;
    String id;
    ArrayList<String> listplaceend = new ArrayList<String>();
    ArrayList<String> listplaceend1 = new ArrayList<String>();
    public String didau,den,ngay;
    public VeFragment(String id) {
        this.id = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_ve, container, false);
        // ánh xạ tại đây
        txt_today = root.findViewById(R.id.txt_today);
        edt_diem_di = root.findViewById(R.id.edt_diem_di);
        edt_diem_den = root.findViewById(R.id.edt_diem_den);
        final Button btn_search = root.findViewById(R.id.btn_search);

        //Sự kiện chỗ này
        setLanguage();
        getDiemDi();
        getDefaultInfor(txt_today);

        edt_diem_di.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mbuilder = new AlertDialog.Builder(getActivity());
                initListView();
                edt_search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().equals("")){

                            for (int i = 0 ; i<bbb.size();i++)
                            {
                                if (!aaa.contains(bbb.get(i)))
                                {
                                    aaa.add(bbb.get(i));
                                }
                            }
                            mView = getLayoutInflater().inflate(R.layout.dialog_diem_di,null);
                            adapter = new ArrayAdapter<String>(getActivity(),R.layout.list_item,R.id.txt_item,aaa);
                            lsv_diem_di.setAdapter(adapter);
                        }else{
                            searchItem(s.toString());
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                lsv_diem_di.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        edt_search.setText(lsv_diem_di.getItemAtPosition(position).toString());
                        //noidi = lsv_diem_di.getItemAtPosition(position).toString();
                        //Toast.makeText(getActivity(), lsv_diem_di.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                mbuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        AlertDialog.Builder mbuilder1 = new AlertDialog.Builder(getActivity());
                        getdiemden(edt_search.getText().toString().trim());
                        initListView2();

                        edt_search_diem_den.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (s.toString().equals("")){

                                    for (int i = 0 ; i<listplaceend1.size();i++)
                                    {
                                        if (!listplaceend.contains(listplaceend1.get(i)))
                                        {
                                            listplaceend.add(listplaceend1.get(i));
                                        }
                                    }
                                    mView1 = getLayoutInflater().inflate(R.layout.dialog_diem_den,null);
                                    adapter1 = new ArrayAdapter<String>(getActivity(),R.layout.list_tem1,R.id.txt_item1,listplaceend);
                                    lsv_diem_den.setAdapter(adapter1);
                                }else{
                                    searchItem1(s.toString());
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                        lsv_diem_den.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                edt_search_diem_den.setText(lsv_diem_den.getItemAtPosition(position).toString());
                            }
                        });
                        mbuilder1.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                edt_diem_di.setText(txtNoiDi.getText().toString());
                                edt_diem_den.setText(edt_search_diem_den.getText().toString());
                                dialog.cancel();
                            }
                        });
                        mbuilder1.setView(mView1);

                        AlertDialog dialog1 = mbuilder1.create();
                        dialog1.setTitle("CHỌN NƠI ĐẾN");
                        dialog1.show();

                    }
                });
                mbuilder.setView(mView);
                AlertDialog dialog = mbuilder.create();
                dialog.setTitle("CHỌN NƠI ĐI");
                dialog.show();
            }
        });




        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String startplace = edt_diem_di.getText().toString();
                final String endplace = edt_diem_den.getText().toString();
                final String day = txt_today.getText().toString().trim();

                String yyyy = day.substring(6,10);
                String mm = day.substring(3,5);
                String dd = day.substring(0,2);
                String ngay = yyyy+"-"+mm+"-"+dd;

                if(startplace.equals("Chọn điểm đi") || endplace.equals("Chọn điểm đến") )
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setTitle("Thông báo");
                    b.setMessage("Vui lòng chọn nơi cần đi!");
                    b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog al = b.create();
                    al.show();

                }else{
                    Intent intent = new Intent(getActivity().getBaseContext(), ScheduleActivity.class);
                    Bundle bundle = new Bundle();
                    // đóng gói kiểu dữ liệu String, Boolean
                    bundle.putString("startplace", startplace);
                    bundle.putString("endplace", endplace);
                    bundle.putString("day", ngay);
                    bundle.putString("id", id);
                    // đóng gói bundle vào intent
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }
            }
        });

        txt_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialogNgayDi();
            }
        });

        return root;
    }

    public void initListView()
    {
        mView = getLayoutInflater().inflate(R.layout.dialog_diem_di,null);
        adapter = new ArrayAdapter<String>(getActivity(),R.layout.list_item,R.id.txt_item,aaa);
        edt_search = mView.findViewById(R.id.edt_search_diem_di);
        lsv_diem_di = mView.findViewById(R.id.lsv_diem_di);
        lsv_diem_di.setAdapter(adapter);
    }
    public void initListView2()
    {
        mView1 = getLayoutInflater().inflate(R.layout.dialog_diem_den,null);
        adapter1 = new ArrayAdapter<String>(getActivity(),R.layout.list_tem1,R.id.txt_item1,listplaceend);
        edt_search_diem_den = mView1.findViewById(R.id.edt_search_diem_den);
        lsv_diem_den = mView1.findViewById(R.id.lsv_diem_den);
        txtNoiDi = mView1.findViewById(R.id.txtNoiDi);
        lsv_diem_den.setAdapter(adapter1);
        txtNoiDi.setText(edt_search.getText().toString());
    }

    public void searchItem(String textToSearch){
        for (String item : bbb){
            if (!item.contains(textToSearch)){
                aaa.remove(item);
            }
        }
        adapter.notifyDataSetChanged();

    }
    public void searchItem1(String textToSearch){
        for (String item : listplaceend1){
            if (!item.contains(textToSearch)){
                listplaceend.remove(item);
            }
        }
        adapter1.notifyDataSetChanged();

    }


    public void getDefaultInfor(TextView txt_today) {
        //lấy ngày hiện tại của hệ thống
        cal = Calendar.getInstance();
        SimpleDateFormat dft = null;
        //Định dạng ngày / tháng /năm
        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(cal.getTime());
        //hiển thị lên giao diện
        txt_today.setText(strDate);
    }

    public void setMindate(CalendarView calendarView) {
        Calendar calendar = Calendar.getInstance();
        calendarView.setMinDate(calendar.getTimeInMillis());
    }

    private void setLanguage() {
        Locale locale = new Locale("vi", "Vietnam");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getApplicationContext().getResources().updateConfiguration(config, null);
    }

    private void getDiemDi() {

        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDi>> call = datVeXe.getNoiDi();
        call.enqueue(new Callback<List<DiemDi>>()
        {
            @Override
            public void onResponse(Call<List<DiemDi>> call, Response<List<DiemDi>> response)
            {
                List<DiemDi> diemDis = response.body();
                for (int i = 0; i <diemDis.size() ; i++)
                {
                    if(!aaa.contains(diemDis.get(i).getDiemdi()))
                    {
                        aaa.add(diemDis.get(i).getDiemdi());
                        bbb.add(diemDis.get(i).getDiemdi());
                    }
                }

                Collections.sort(aaa);
                Collections.sort(bbb);
            }

            @Override
            public void onFailure(Call<List<DiemDi>> call, Throwable t)
            {

            }
        });


    }

    private void getdiemden(String bb) {

        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDen>> call = datVeXe.getNoiDen(bb);
        call.enqueue(new Callback<List<DiemDen>>() {
            @Override
            public void onResponse(Call<List<DiemDen>> call, Response<List<DiemDen>> response) {
                List<DiemDen> diemDens = response.body();
                for (int i = 0; i < diemDens.size(); i++) {
                    if (!listplaceend.contains(diemDens.get(i).getDiemden())) {
                        listplaceend.add(diemDens.get(i).getDiemden());
                        listplaceend1.add(diemDens.get(i).getDiemden());
                    }
                }
                for (int j = 0 ; j < listplaceend.size() ;j++)
                {
                    Log.i("list: ",listplaceend.get(j));
                }
                Collections.sort(listplaceend);
                Collections.sort(listplaceend1);

            }

            @Override
            public void onFailure(Call<List<DiemDen>> call, Throwable t) {

            }
        });
    }

    public void showDatePickerDialogNgayDi()
    {
        DatePickerDialog.OnDateSetListener callback=new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                //Mỗi lần thay đổi ngày tháng năm thì cập nhật lại TextView Date
                String day, month1;
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else
                    day = String.valueOf(dayOfMonth);
                if (monthOfYear < 10) {
                    month1 = "0" + (monthOfYear + 1);
                } else {
                    month1 = String.valueOf(monthOfYear);
                }
                txt_today.setText(
                        (day) +"/"+(month1)+"/"+year);
                /*txtNgayThue.setText(
                        (dayOfMonth) +"/"+(monthOfYear+1)+"/"+year);*/
                //Lưu vết lại biến ngày hoàn thành
                cal.set(year, monthOfYear, dayOfMonth);
                dateFinish=cal.getTime();
            }
        };
        //các lệnh dưới này xử lý ngày giờ trong DatePickerDialog
        //sẽ giống với trên TextView khi mở nó lên
        String s = txt_today.getText()+"";
        String strArrtmp[]=s.split("/");
        int ngay=Integer.parseInt(strArrtmp[0]);
        int thang=Integer.parseInt(strArrtmp[1])-1;
        int nam=Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                getActivity(),
                callback, nam, thang, ngay);
        pic.setTitle("CHỌN NGÀY ĐI");
        pic.show();


    }

   /* @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("noidi", edt_diem_di.getText().toString().trim());
        savedInstanceState.putString("noiden", edt_diem_den.getText().toString().trim());
        savedInstanceState.putString("ngaydi", txt_today.getText().toString().trim());
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        didau = savedInstanceState.getString("noidi");
        den = savedInstanceState.getString("noiden");
        ngay = savedInstanceState.getString("ngay");
    }*/
}
