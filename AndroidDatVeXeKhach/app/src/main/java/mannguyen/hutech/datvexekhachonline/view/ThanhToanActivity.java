package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.R;

public class ThanhToanActivity extends AppCompatActivity {
    private TextView txt_ngay_khoi_hanh,txt_gio_khoi_hanh,txt_gia_ve,txt_thanh_toan,
            txt_ten_khach_hang,txt_dien_thoai,txt_dia_chi,txt_tong_so_ve,txt_tong_tien,txt_numbercs;
    private Button btn_xac_nhan;
    private RadioGroup radiogr;
    private RadioButton rdoTienMat, rdoMoMo, rdoVNPay, radioButton;
    String machuyen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanh_toan);
        AnhXa();
        setUI();
    }

    private void AnhXa() {

        txt_ngay_khoi_hanh = findViewById(R.id.txt_ngay_khoi_hanh);
        txt_gio_khoi_hanh = findViewById(R.id.txt_gio_khoi_hanh);
        txt_gia_ve = findViewById(R.id.txt_gia_ve);
        txt_ten_khach_hang = findViewById(R.id.txt_ten_khach_hang);
        txt_dien_thoai = findViewById(R.id.txt_dien_thoai);
        txt_dia_chi = findViewById(R.id.txt_dia_chi);
        txt_tong_so_ve = findViewById(R.id.txt_tong_so_ve);
        txt_tong_tien = findViewById(R.id.txt_tong_tien);

        btn_xac_nhan = findViewById(R.id.btn_xac_nhan);
    }
    private void setUI(){
        final Intent intent = getIntent();
        final String startplace = intent.getStringExtra("start");
        final String endplace = intent.getStringExtra("end");
        final String day = intent.getStringExtra("day");
        final String time = intent.getStringExtra("time");
        final String namecar = intent.getStringExtra("namecar");
        String price = intent.getStringExtra("price");
        final String tenKH = intent.getStringExtra("hoten");
        final String sdt = intent.getStringExtra("sdt");
        final String diachi = intent.getStringExtra("diachi");
        final String diemdon = intent.getStringExtra("diemdon");
        String sove = intent.getStringExtra("sove");
        final String tongtien = intent.getStringExtra("tongtien");
        machuyen = intent.getStringExtra("machuyen");
        final String soghe = intent.getStringExtra("soghe");

       // Toast.makeText(this, machuyen, Toast.LENGTH_SHORT).show();

        txt_ngay_khoi_hanh.setText(day);
        txt_gio_khoi_hanh.setText(time);
        int gia = Integer.valueOf(price);
        txt_gia_ve.setText(changeNumber(gia));
        txt_tong_so_ve.setText(sove);
        final int tong = Integer.valueOf(tongtien);
        txt_tong_tien.setText(changeNumber(tong));


        txt_ten_khach_hang.setText(tenKH);
        txt_dien_thoai.setText(sdt);
        txt_dia_chi.setText(diachi);
        //final ArrayList<String> soghe = new ArrayList<String>();

        btn_xac_nhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radiogr = findViewById(R.id.radiogr);
                rdoVNPay = findViewById(R.id.rdoVNPay);
                rdoTienMat = findViewById(R.id.rdoTienMat);

                int i = radiogr.getCheckedRadioButtonId();

                switch (i){
                    case R.id.rdoTienMat:
                        Intent intent1 = new Intent(ThanhToanActivity.this, SaveQRCodeActivity.class);
                        Bundle bundle = new Bundle();

                        bundle.putString("diemdon",diemdon);
                        bundle.putString("ten", tenKH);
                        bundle.putString("sdt", sdt);
                        bundle.putString("dc", diachi);
                        bundle.putString("soghe", soghe);
                        bundle.putString("mc", machuyen);
                        bundle.putString("ngaydi",day);
                        bundle.putString("noidi", startplace);
                        bundle.putString("noiden",endplace);
                        bundle.putString("nhaxe",namecar);
                        bundle.putString("gio",time);
                        intent1.putExtras(bundle);

                        startActivity(intent1);
                        break;
                    case R.id.rdoVNPay:
                        break;
                    default:
                        Toast.makeText(ThanhToanActivity.this, "Vui lòng chọn hình thức thanh toán", Toast.LENGTH_SHORT).show();
                        break;
                }

            }


        });


    }


    public String changeNumber(int number) {
        NumberFormat currentLocale = NumberFormat.getInstance();
        Locale localeEN = new Locale("en", "EN");
        NumberFormat en = NumberFormat.getInstance(localeEN);
        String str1 = en.format(number);
        return str1;
    }

}

