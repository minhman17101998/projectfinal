package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.adapter.CustomAdapter;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.CustomView;
import mannguyen.hutech.datvexekhachonline.model.DiemDon;
import mannguyen.hutech.datvexekhachonline.model.KhachHang;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static mannguyen.hutech.datvexekhachonline.R.drawable.hetghe;

public class ChooseSeatActivity extends AppCompatActivity {

    private TextView txt_name_car_infor, txt_start_infor, txt_end_infor, txt_time_start_infor, txt_date_start_infor;;

    private GridView gridView;

    private Button btn_back, btn_continue;

    private ArrayList<String> numbers = new ArrayList<>();

    private ArrayList<String> selectedStrings;

    int KT = 0;

    public String price, machuyen;

    private Spinner  spn_diem_len_xe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_seat);

        AnhXa();

        getInforCar();

        addArray();

        onClickButton();

    }

    private void getInforCar()
    {
        Intent intent = getIntent();
        String startplace = intent.getStringExtra("start");
        String endplace = intent.getStringExtra("end");
        String day = intent.getStringExtra("day");
        String time = intent.getStringExtra("time");
        String namecar = intent.getStringExtra("namecar");

        price = intent.getStringExtra("price");
        txt_start_infor.setText(startplace);
        txt_end_infor.setText(endplace);
        txt_name_car_infor.setText(namecar);
        txt_date_start_infor.setText(day);
        txt_time_start_infor.setText(time);
        machuyen = intent.getStringExtra("machuyen");

        getDiemDon(spn_diem_len_xe);
    }

    private void AnhXa()
    {
        txt_name_car_infor = findViewById(R.id.txt_name_car_infor);
        txt_start_infor = findViewById(R.id.txt_start_infor);
        txt_end_infor = findViewById(R.id.txt_end_infor);
        txt_time_start_infor = findViewById(R.id.txt_time_start_infor);
        txt_date_start_infor = findViewById(R.id.txt_date_start_infor);

        btn_continue = findViewById(R.id.btn_continue);

        gridView = findViewById(R.id.grid_view_choose_seat);

        spn_diem_len_xe = findViewById(R.id.spn_diem_len_xe);

        selectedStrings = new ArrayList<>();
    }

    private void addArray()
    {
        for (int i = 0; i < 28; i++)
        {
            numbers.add(i+1 + "");
        }

        final ArrayList<String> listSoGhe = new ArrayList<>();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<KhachHang>> call = datVeXe.getSoGhe(machuyen);
        call.enqueue(new Callback<List<KhachHang>>()
        {
            @Override
            public void onResponse(Call<List<KhachHang>> call, Response<List<KhachHang>> response)
            {
                List<KhachHang> khachHangs = response.body();
                for (int i = 0; i < khachHangs.size(); i++)
                {
                    listSoGhe.add(khachHangs.get(i).getSoghe());
                }

                Collections.sort(listSoGhe);

                /*for (int a = 0 ; a < numbers.size() ; a++)
                {
                    for (int b = 0 ; b < listSoGhe.size() ; b++)
                    {
                        if (numbers.get(a).equals(listSoGhe.get(b)))
                        {
                            numbers.remove(a);
                        }
                    }
                }*/

                final CustomAdapter adapter = new CustomAdapter(ChooseSeatActivity.this, numbers,listSoGhe);

                gridView.setAdapter(adapter);

                gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onGlobalLayout() {

                        for (int i = 0 ; i < gridView.getCount() ; i++)
                        {
                            for (int j = 0 ; j < listSoGhe.size() ; j++)
                            {
                                if (gridView.getItemAtPosition(i).equals(listSoGhe.get(j)))
                                {
                                    View view  = gridView.getChildAt(i);
                                    view.setBackgroundColor(R.color.red);
                                }
                            }

                        }
                    }
                });

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id)
                    {


                                int selectedIndex = adapter.selectedPositions.indexOf(position);

                                if (selectedIndex > -1)
                                {
                                    adapter.selectedPositions.remove(selectedIndex);
                                    selectedStrings.remove((String) parent.getItemAtPosition(position));
                                    KT--;
                                    ((CustomView) v).display(false);

                                }
                                else if (adapter.selectedPositions.size() > 3)
                                {
                                    AlertDialog.Builder b = new AlertDialog.Builder(ChooseSeatActivity.this);
                                    b.setTitle("Thông báo");
                                    b.setMessage("Bạn chỉ được phép chọn tối đa 4 ghế!");
                                    b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog al = b.create();
                                    al.show();
                                }
                                else
                                {
                                    adapter.selectedPositions.add(position);
                                    selectedStrings.add((String) parent.getItemAtPosition(position));
                                    KT++;
                                    ((CustomView) v).display(true);
                                }
                            }





                });
            }

            @Override
            public void onFailure(Call<List<KhachHang>> call, Throwable t)
            {
                Log.e("Loi", t.getMessage());
            }
        });
    }

    private void onClickButton()
    {
        //final String diemdon = spn_diem_len_xe.getSelectedItem().toString();
        btn_continue.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (spn_diem_len_xe.getSelectedItem().toString().equals("Chọn điểm đón"))
                {
                    AlertDialog.Builder b1 = new AlertDialog.Builder(ChooseSeatActivity.this);
                    b1.setTitle("Thông báo");
                    b1.setMessage("Bạn chưa chọn điểm đón!");
                    b1.setNegativeButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.cancel();
                        }
                    });
                    AlertDialog a2 = b1.create();
                    a2.show();
                }
                else if (!(KT == 0))
                {

                        Intent intent = new Intent(ChooseSeatActivity.this, CustomorInforActivity.class);
                        Bundle bundle = new Bundle();
                        // đóng gói kiểu dữ liệu String, Boolean
                        bundle.putString("diemdon",spn_diem_len_xe.getSelectedItem().toString());
                        bundle.putString("start", txt_start_infor.getText().toString());
                        bundle.putString("end", txt_end_infor.getText().toString());
                        bundle.putString("day", txt_date_start_infor.getText().toString());
                        bundle.putString("time", txt_time_start_infor.getText().toString());
                        bundle.putString("namecar", txt_name_car_infor.getText().toString());
                        bundle.putString("price", price);
                        bundle.putString("machuyen", machuyen);
                        intent.putStringArrayListExtra("SELECTED_LETTER", selectedStrings);
                        // đóng gói bundle vào intent
                        intent.putExtras(bundle);
                        startActivity(intent);


                }
                /*else if (diemdon.equals("Chọn điểm đón"))
                {

                }*/
                else
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(ChooseSeatActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa chọn chỗ ngồi!");
                    b.setNegativeButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.cancel();
                        }
                    });
                    AlertDialog al = b.create();
                    al.show();
                }
            }
        });
    }

    private void getDiemDon(final Spinner spn_diem_len_xe) {
        final ArrayList<String> listplaceend = new ArrayList<String>();
        String noidi = txt_start_infor.getText().toString();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDon>> call = datVeXe.getDiemDon(noidi);
        call.enqueue(new Callback<List<DiemDon>>() {
            @Override
            public void onResponse(Call<List<DiemDon>> call, Response<List<DiemDon>> response) {
                List<DiemDon> diemDens = response.body();
                for (int i = 0; i <diemDens.size() ; i++)
                {
                    if(!listplaceend.contains(diemDens.get(i).getDiemdon()))
                    {
                        listplaceend.add(diemDens.get(i).getDiemdon());
                    }
                }
                //Collections.sort(listplaceend);
                listplaceend.add(0,"Chọn điểm đón");
                ArrayAdapter arrayAdapter = new ArrayAdapter(ChooseSeatActivity.this, android.R.layout.simple_spinner_item, listplaceend);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spn_diem_len_xe.setAdapter(arrayAdapter);


            }

            @Override
            public void onFailure(Call<List<DiemDon>> call, Throwable t) {

            }
        });


    }
}
