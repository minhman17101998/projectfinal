package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.adapter.NoiDiAdapter;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiemDiActivity extends AppCompatActivity
{
    ListView lsvNoiDi;
    NoiDiAdapter adapter;
    List<DiemDi> list;
    //private ArrayList<String> listNoiDi = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diem_di);

        lsvNoiDi = findViewById(R.id.lsvNoiDi);

        readDB();

    }

    private void readDB()
    {
        //final ArrayList<String> aaa = new ArrayList<String>();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDi>> call = datVeXe.getNoiDi();
        call.enqueue(new Callback<List<DiemDi>>()
        {
            @Override
            public void onResponse(Call<List<DiemDi>> call, Response<List<DiemDi>> response)
            {
                list = response.body();
                adapter = new NoiDiAdapter(DiemDiActivity.this,list);
                lsvNoiDi.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<DiemDi>> call, Throwable t)
            {

            }
        });
    }


}
