package mannguyen.hutech.datvexekhachonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.model.Schedule;

public class NoiDiAdapter extends BaseAdapter {
    private Activity context;
    private List<DiemDi> list;
    RecyclerView.ViewHolder vh = null;

    public NoiDiAdapter(Activity context, List<DiemDi> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder {

        public TextView txtNoiDi;


    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            vi = inflater.inflate(R.layout.noidi, null);

            holder = new ViewHolder();
            holder.txtNoiDi = vi.findViewById(R.id.txt_noi_di);


            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();

        // now set your text view here like

        // holder.tvName.setText("Bla Bla Bla");

        holder.txtNoiDi.setText(list.get(position).getDiemdi());

        // return your view
        return vi;
    }
}
