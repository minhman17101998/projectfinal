package mannguyen.hutech.datvexekhachonline.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import mannguyen.hutech.datvexekhachonline.Database.Database;
import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.adapter.AdapterAnh;
import mannguyen.hutech.datvexekhachonline.model.Anh;

/**
 * A simple {@link Fragment} subclass.

 *  *  * create an instance of this fragment.
 */
public class MyTicketFragment extends Fragment {
    final String DATABASE_NAME = "datvexe.sqlite";
    SQLiteDatabase database;
    ListView lsv_ve;
    ArrayList<Anh> list;
    AdapterAnh adapterAnh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_my_ticket, container, false);
        lsv_ve = root.findViewById(R.id.lsv_ve);
        addControls();
        readData();
        return  root;
    }

    private void addControls()
    {
        list = new ArrayList<>();
        adapterAnh = new AdapterAnh(getActivity(),list);
        lsv_ve.setAdapter(adapterAnh);
    }

    private void readData()
    {
        database = Database.initDatabase(getActivity(),DATABASE_NAME);
        Cursor cursor = database.rawQuery("SELECT * FROM maqr",null);
        list.clear();
        for (int i = 0;i<cursor.getCount();i++)
        {
            cursor.moveToPosition(i);
            int id = cursor.getInt(0);
            byte[] maqr = cursor.getBlob(1);
            String nhaxe = cursor.getString(2);
            String diemdi = cursor.getString(3);
            String diemden = cursor.getString(4);
            String ngaydi = cursor.getString(5);
            String giodi = cursor.getString(6);
            String soghe = cursor.getString(7);
            list.add(new Anh(id,maqr,nhaxe,diemdi,diemden,ngaydi,giodi,soghe));
        }
        adapterAnh.notifyDataSetChanged();
    }
}
