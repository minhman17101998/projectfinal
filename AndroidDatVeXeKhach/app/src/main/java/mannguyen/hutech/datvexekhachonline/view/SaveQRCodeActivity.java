package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import mannguyen.hutech.datvexekhachonline.Database.Database;
import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.RequestHandler;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.KhachHang;
import mannguyen.hutech.datvexekhachonline.model.Tickets;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveQRCodeActivity extends AppCompatActivity {
    public static final String UPLOAD_URL = "http://mbvlogs.000webhostapp.com/uploadimage.php";
    final String DATABASE_NAME = "datvexe.sqlite";
    ImageView img_qr_code;
    Button btn_luu_qr_code;
    QRGEncoder qrgEncoder;
    Bitmap bitmap;
    int machuyen,makh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_q_r_code);

        img_qr_code = findViewById(R.id.img_qr_code);
        btn_luu_qr_code = findViewById(R.id.btn_luu_qr_code);

        saveKH();
        getMaKH();

        btn_luu_qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                luu();
                addTicket(makh);
                //uploadImage();
            }
        });

    }
    public void TaoQR(int makhach)
    {
        Intent intent = getIntent();

        final String ten = intent.getStringExtra("ten");
        final String ma = intent.getStringExtra("mc");
        machuyen = Integer.valueOf(ma);
        String soghe = intent.getStringExtra("soghe");
        final String day = intent.getStringExtra("ngaydi");
        final String noidi = intent.getStringExtra("noidi");
        final String noiden = intent.getStringExtra("noiden");
        final String nhaxe = intent.getStringExtra("nhaxe");
        final String gio = intent.getStringExtra("gio");
        String yyyy = day.substring(6,10);
        String mm = day.substring(3,5);
        String dd = day.substring(0,2);
        String ngay = dd+mm+yyyy;

        String tenve = machuyen+String.valueOf(makhach)+ngay;

        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallermension = width<height?width:height;
        smallermension = smallermension * 3/4;
        qrgEncoder = new QRGEncoder("Mã vé: "+ tenve
                + "\n" +"Nhà xe: "+ nhaxe
                +"\n"+"Lộ trình: " + noidi +" ---> " +noiden
                + "\n" +"Ngày đi: "+ day
                + "\n" +"Giờ đi: "+ gio
                +"\n"+"Tên khách hàng: " + ten
                + "\n" +"Số ghế: "+ soghe
                ,null, QRGContents.Type.TEXT,smallermension);

        try {
            bitmap = qrgEncoder.encodeAsBitmap();
            img_qr_code.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    private void addTicket(int makhach)
    {
        Intent intent = getIntent();

        final String ma = intent.getStringExtra("mc");
        machuyen = Integer.valueOf(ma);
        final String day = intent.getStringExtra("ngaydi");

        String yyyy = day.substring(6,10);
        String mm = day.substring(3,5);
        String dd = day.substring(0,2);
        String ngay = dd+mm+yyyy;
        Toast.makeText(this, ngay, Toast.LENGTH_SHORT).show();
        String tenve = machuyen+String.valueOf(makhach)+ngay;

        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<Tickets> call = datVeXe.addTicket(tenve,makhach,machuyen);
        call.enqueue(new Callback<Tickets>() {
            @Override
            public void onResponse(Call<Tickets> call, Response<Tickets> response) {
                if(response.isSuccessful() && response.body() != null){
                    Boolean success =  response.body().getSuccess();
                    if (success){
                        Log.i("Thong bao:","Them ve Thanh cong");
                    }else{
                        Toast.makeText(SaveQRCodeActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    }}
            }

            @Override
            public void onFailure(Call<Tickets> call, Throwable t) {
                Log.i("Loi them ve: ",t.getMessage());
            }
        });
    }
    private void getMaKH()
    {
        Intent intent = getIntent();
        final String ten = intent.getStringExtra("ten");
        String sdt = intent.getStringExtra("sdt");
        String diachi = intent.getStringExtra("dc");
        final String ma = intent.getStringExtra("mc");
        final int machuyen = Integer.valueOf(ma);
        String soghe = intent.getStringExtra("soghe");

        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<KhachHang>> call = datVeXe.getMaKH(ten,sdt,diachi,machuyen,soghe);
        call.enqueue(new Callback<List<KhachHang>>() {
            @Override
            public void onResponse(Call<List<KhachHang>> call, Response<List<KhachHang>> response) {
                if (response.isSuccessful())
                {
                    List<KhachHang> khachHangs = response.body();
                    for (int i = 0; i<khachHangs.size();i++)
                    {
                        makh = khachHangs.get(i).getMakhachhang();
                        Log.i("MAKH: ",String.valueOf(makh));
                        //addTicket(makh);
                        TaoQR(makh);
                    }
                }else {
                    Toast.makeText(SaveQRCodeActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<KhachHang>> call, Throwable t) {

            }
        });
    }

    private  byte[] getByteArray(ImageView img)
    {
        BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
        Bitmap bmp = drawable.getBitmap();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    private void luu()
    {
        Intent intent = getIntent();

        final String ma = intent.getStringExtra("mc");
        machuyen = Integer.valueOf(ma);
        String soghe = intent.getStringExtra("soghe");
        final String day = intent.getStringExtra("ngaydi");
        final String noidi = intent.getStringExtra("noidi");
        final String noiden = intent.getStringExtra("noiden");
        final String nhaxe = intent.getStringExtra("nhaxe");
        final String gio = intent.getStringExtra("gio");
        byte[] anh = getByteArray(img_qr_code);

        ContentValues contentValues = new ContentValues();
        contentValues.put("tennhaxe",nhaxe);
        contentValues.put("diemdi",noidi);
        contentValues.put("diemden",noiden);
        contentValues.put("ngaydi",day);
        contentValues.put("giodi",gio);
        contentValues.put("soghe",soghe);
        contentValues.put("img",anh);

        SQLiteDatabase database = Database.initDatabase(this,DATABASE_NAME);
        database.insert("maqr",null,contentValues);

        AlertDialog.Builder b = new AlertDialog.Builder(SaveQRCodeActivity.this);
        b.setTitle("Thông báo");
        b.setMessage("Lưu Thành Công");
        b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(SaveQRCodeActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    private void saveKH()
    {
        Intent intent = getIntent();
        String ten = intent.getStringExtra("ten");
        String sdt = intent.getStringExtra("sdt");
        String diachi = intent.getStringExtra("dc");
        final String ma = intent.getStringExtra("mc");
        int machuyen = Integer.valueOf(ma);
        String soghe = intent.getStringExtra("soghe");
        String diemdon = intent.getStringExtra("diemdon");
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<KhachHang> call = datVeXe.saveKH(ten,sdt,diachi,Integer.valueOf(machuyen),soghe,diemdon);
        call.enqueue(new Callback<KhachHang>() {
            @Override
            public void onResponse(Call<KhachHang> call, Response<KhachHang> response) {
                if(response.isSuccessful() && response.body() != null){
                    Boolean success =  response.body().getSuccess();
                    if (success){
                        Log.i("Thong bao:","Them KH Thanh cong");
                    }else{
                        Toast.makeText(SaveQRCodeActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    }}
            }

            @Override
            public void onFailure(Call<KhachHang> call, Throwable t) {
                Log.i("Loi them KH: ",t.getMessage());
            }
        });

    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {

            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(SaveQRCodeActivity.this, "Uploading...", null,true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);

                HashMap<String,String> data = new HashMap<>();

                data.put("image", uploadImage);
                data.put("makh", String.valueOf(makh));
                String result = rh.sendPostRequest(UPLOAD_URL,data);

                return result;
            }
        }

        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }
}
