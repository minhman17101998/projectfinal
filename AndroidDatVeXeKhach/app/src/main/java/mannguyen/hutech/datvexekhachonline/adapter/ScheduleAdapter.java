package mannguyen.hutech.datvexekhachonline.adapter;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ItemClickLisner;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import mannguyen.hutech.datvexekhachonline.view.ChooseSeatActivity;


public class ScheduleAdapter  extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {


    private List<Schedule> schedules;
    private Context context;

    public ScheduleAdapter(List<Schedule> schedules, Context context) {
        this.schedules = schedules;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_schedule,parent,false);



        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Schedule schedule = schedules.get(position);

        int gia = Integer.valueOf(schedule.getGiave());
        String day = schedule.getNgaydi();
        String yyyy = day.substring(0,4);
        String mm = day.substring(5,7);
        String dd = day.substring(8,10);
        String ngay = dd+"/"+mm+"/"+yyyy;
        holder.txt_date_start.setText(ngay);
        holder.txt_time_start.setText(schedule.getGiodi());
        holder.txt_name_car.setText(schedule.getNhaxe()+"");
        holder.txt_ticket_cost.setText(changeNumber(gia)+"");

        holder.setItemClickListener(new ItemClickLisner() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent intent = new Intent(context, ChooseSeatActivity.class);
                Bundle bundle = new Bundle();
                // đóng gói kiểu dữ liệu String, Boolean
                bundle.putString("start", schedule.getDiemdi());
                bundle.putString("end", schedule.getDiemden());
                bundle.putString("day", holder.txt_date_start.getText().toString());
                bundle.putString("time", holder.txt_time_start.getText().toString());
                bundle.putString("namecar", holder.txt_name_car.getText().toString());
                bundle.putString("price", String.valueOf(schedule.getGiave()));
                bundle.putString("machuyen", String.valueOf(schedule.getMachuyen()));
                // đóng gói bundle vào intent
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ItemClickLisner itemClickListener;

        public TextView txt_start,txt_end,txt_time_start,txt_date_start,txt_name_car,txt_ticket_cost;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_time_start = itemView.findViewById(R.id.txt_time_start);
            txt_date_start = itemView.findViewById(R.id.txt_date_start);
            txt_name_car = itemView.findViewById(R.id.txt_name_car);
            txt_ticket_cost = itemView.findViewById(R.id.txt_ticket_cost);

            itemView.setOnClickListener(this);
        }
        //Tạo setter cho biến itemClickListenenr
        public void setItemClickListener(ItemClickLisner itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),false);
            // Gọi interface , false là vì đây là onClick
        }
    }
    public String changeNumber(int number){
        NumberFormat currentLocale = NumberFormat.getInstance();
        Locale localeEN = new Locale("en", "EN");
        NumberFormat en = NumberFormat.getInstance(localeEN);
        String str1 = en.format(number);
        return str1;
    }
}