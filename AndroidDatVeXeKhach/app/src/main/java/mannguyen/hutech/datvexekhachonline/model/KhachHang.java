package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class KhachHang {
    @SerializedName("makhachhang")
    private int makhachhang;
    @SerializedName("tenkhachhang")
    private String tenkhachhang;
    @SerializedName("sodienthoai")
    private String sodienthoai;
    @SerializedName("diachi")
    private String diachi;
    @SerializedName("machuyen")
    private int machuyen;
    @SerializedName("soghe")
    private String soghe;
    @SerializedName("diemdon")
    private String diemdon;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;

    public int getMakhachhang() {
        return makhachhang;
    }

    public void setMakhachhang(int makhachhang) {
        this.makhachhang = makhachhang;
    }

    public String getTenkhachhang() {
        return tenkhachhang;
    }

    public void setTenkhachhang(String tenkhachhang) {
        this.tenkhachhang = tenkhachhang;
    }

    public String getSodienthoai() {
        return sodienthoai;
    }

    public void setSodienthoai(String sodienthoai) {
        this.sodienthoai = sodienthoai;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public int getMachuyen() {
        return machuyen;
    }

    public void setMachuyen(int machuyen) {
        this.machuyen = machuyen;
    }

    public String getSoghe() {
        return soghe;
    }

    public void setSoghe(String soghe) {
        this.soghe = soghe;
    }

    public String getDiemdon() {
        return diemdon;
    }

    public void setDiemdon(String diemdon) {
        this.diemdon = diemdon;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
