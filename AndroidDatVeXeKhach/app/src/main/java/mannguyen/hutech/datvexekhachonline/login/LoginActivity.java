package mannguyen.hutech.datvexekhachonline.login;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.UserID;
import mannguyen.hutech.datvexekhachonline.model.users;
import mannguyen.hutech.datvexekhachonline.view.MainActivity;
import mannguyen.hutech.datvexekhachonline.view.SaveQRCodeActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView txtTen,txtTaoTK;
    Button btnLogin;
    EditText edt_username,edt_password;
    public  String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AnhXa();
        addControls();
    }

    private void addControls() {
        txtTaoTK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void AnhXa() {
        txtTen = findViewById(R.id.txtTen);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/font.TTF");
        txtTen.setTypeface(typeface);
        txtTaoTK = findViewById(R.id.txtTaoTK);

        btnLogin = findViewById(R.id.btnLogin);

        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
    }

    private void login()
    {
        final String tendangnhap = edt_username.getText().toString().trim();
        String matkhau = edt_password.getText().toString().trim();
        if (tendangnhap.equals(""))
        {
            edt_username.setError("Tên đăng nhập không được để trống");
        } else if (matkhau.equals(""))
        {
            edt_password.setError("Mật khẩu không được để trống");
        } else {
            ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
            Call<users> call = datVeXe.getLogin(tendangnhap,matkhau);
            call.enqueue(new Callback<users>() {
                @Override
                public void onResponse(Call<users> call, Response<users> response) {
                    if (response.body()!=null)
                    {
                        users userss = response.body();

                        if (userss.isSuccess())
                        {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("username",tendangnhap);
                            startActivity(intent);
                        }
                        else{
                            AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                            b.setTitle("Thông báo");
                            b.setMessage("Tên đăng nhập hoặc mật khẩu không đúng!");
                            b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            AlertDialog al = b.create();
                            al.show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<users> call, Throwable t) {
                    Log.i("TB1:",t.getMessage());
                }
            });
        }

    }


}
