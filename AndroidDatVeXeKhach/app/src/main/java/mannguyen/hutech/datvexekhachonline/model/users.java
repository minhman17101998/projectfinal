package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class users {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    @SerializedName("id")
    private String id;
    @SerializedName("email")
    private String email;
    @SerializedName("success")
    private boolean success;
    @SerializedName("response")
    private String response;

    public boolean isSuccess() {
        return success;
    }

    public String getResponse() {
        return response;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
