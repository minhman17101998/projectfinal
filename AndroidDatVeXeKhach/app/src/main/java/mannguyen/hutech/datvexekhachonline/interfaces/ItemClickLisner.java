package mannguyen.hutech.datvexekhachonline.interfaces;

import android.view.View;

public interface ItemClickLisner {
    void onClick(View view, int position, boolean isLongClick);
}
