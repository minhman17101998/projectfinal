package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.Database.Database;
import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.UserID;
import mannguyen.hutech.datvexekhachonline.model.DiemDon;
import mannguyen.hutech.datvexekhachonline.model.InforCus;
import mannguyen.hutech.datvexekhachonline.model.Tinh;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomorInforActivity extends AppCompatActivity {
    final String DATABASE_NAME = "datvexe.sqlite";

    private TextView txt_name_car_infor, txt_start_infor,
            txt_end_infor, txt_time_start_infor,
            txt_date_start_infor,txtdiemdon;
    private TextView txt_price_ticketcs, txt_numbercs, txt_total_pricecs;
    private Button btn_continue;
    private TextView txtHoTen, txtSDT, txtSoNha;
    int dem, gia, tong;
    String machuyen, startplace, endplace,idd,id;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customor_infor);

        AnhXa();

        getIntentData();

        getInforCar();


        addControls();

        getThongTin();

    }

    private void getInforCar() {
        Intent intent = getIntent();
        startplace = intent.getStringExtra("start");
        endplace = intent.getStringExtra("end");
        String day = intent.getStringExtra("day");
        String time = intent.getStringExtra("time");
        String namecar = intent.getStringExtra("namecar");
        String price = intent.getStringExtra("price");
        String diemdon = intent.getStringExtra("diemdon");
        machuyen = intent.getStringExtra("machuyen");
        int gia = Integer.valueOf(price);

        txt_price_ticketcs.setText(changeNumber(gia));
        txt_start_infor.setText(startplace);
        txt_end_infor.setText(endplace);
        txt_name_car_infor.setText(namecar);
        txt_date_start_infor.setText(day);
        txt_time_start_infor.setText(time);
        txtdiemdon.setText(diemdon);
        //Toast.makeText(this, dem, Toast.LENGTH_SHORT).show();

    }

    private void AnhXa() {
        txt_name_car_infor = findViewById(R.id.txt_name_car_inforcs);
        txt_start_infor = findViewById(R.id.txt_start_inforcs);
        txt_end_infor = findViewById(R.id.txt_end_inforcs);
        txt_time_start_infor = findViewById(R.id.txt_time_start_inforcs);
        txt_date_start_infor = findViewById(R.id.txt_date_start_inforcs);
        txt_price_ticketcs = findViewById(R.id.txt_price_ticketcs);
        txt_numbercs = findViewById(R.id.txt_numbercs);
        txt_total_pricecs = findViewById(R.id.txt_total_pricecs);
        txtdiemdon = findViewById(R.id.txt_diemdon);

        btn_continue = findViewById(R.id.btn_continuecs);

        txtHoTen = findViewById(R.id.txt_Name_customer);
        txtSDT = findViewById(R.id.txt_Phone_customer);
        txtSoNha = findViewById(R.id.txtDiaChi);



    }

    public void getIntentData() {
        Intent intent = getIntent();
        String price = intent.getStringExtra("price");
        ArrayList<String> stringArrayList = getIntent().getStringArrayListExtra("SELECTED_LETTER");

        dem = stringArrayList.size();
        gia = Integer.valueOf(price);
        tong = dem * gia;
        txt_total_pricecs.setText(changeNumber(tong));

        assert stringArrayList != null;
        if (stringArrayList.size() > 0) {
            for (int i = 0; i < stringArrayList.size(); i++) {
                if (i < stringArrayList.size() - 1) {
                    txt_numbercs.setText(txt_numbercs.getText() + stringArrayList.get(i) + ", ");

                } else {
                    txt_numbercs.setText(txt_numbercs.getText() + stringArrayList.get(i));
                }
            }
        }
    }


    public String changeNumber(int number) {
        NumberFormat currentLocale = NumberFormat.getInstance();
        Locale localeEN = new Locale("en", "EN");
        NumberFormat en = NumberFormat.getInstance(localeEN);
        String str1 = en.format(number);
        return str1;
    }



    public void addControls() {

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            Intent intent = new Intent(CustomorInforActivity.this, ThanhToanActivity.class);
                            Bundle bundle = new Bundle();

                            bundle.putString("diemdon",txtdiemdon.getText().toString());
                            bundle.putString("day", txt_date_start_infor.getText().toString());
                            bundle.putString("time", txt_time_start_infor.getText().toString());
                            bundle.putString("namecar", txt_name_car_infor.getText().toString());
                            bundle.putString("price", String.valueOf(gia));
                            bundle.putString("start", startplace);
                            bundle.putString("end", endplace);
                            bundle.putString("soghe", txt_numbercs.getText().toString());
                            bundle.putString("machuyen", machuyen);
                            bundle.putString("httt", "Tiền mặt");
                            bundle.putString("sove", String.valueOf(dem));
                            bundle.putString("tongtien", String.valueOf(tong));
                            bundle.putString("hoten", txtHoTen.getText().toString().trim());
                            bundle.putString("sdt", txtSDT.getText().toString().trim());
                            bundle.putString("diachi", txtSoNha.getText().toString().trim());
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }

                });
            }
        });

    }
    private void getThongTin()
    {

        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<InforCus>> call = datVeXe.getInforUser(id);
        call.enqueue(new Callback<List<InforCus>>() {
            @Override
            public void onResponse(Call<List<InforCus>> call, Response<List<InforCus>> response) {
                if (response.isSuccessful() && response != null) {
                    List<InforCus> inforCuses = response.body();
                    for (int i = 0; i < inforCuses.size(); i++) {
                        txtHoTen.setText(inforCuses.get(i).getTenkhachhang());
                        txtSDT.setText(inforCuses.get(i).getSdt());
                        txtSoNha.setText(inforCuses.get(i).getDiachi());

                    }
                }
            }

            @Override
            public void onFailure(Call<List<InforCus>> call, Throwable t) {

            }
        });
    }
}
