package mannguyen.hutech.datvexekhachonline.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.CustomView;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> number_chair;
    private ArrayList<String> hetcho;
    public List selectedPositions;


    public CustomAdapter(Context context, ArrayList<String> number_chair,ArrayList<String> hetcho) {
        this.context = context;
        this.number_chair = number_chair;
        this.hetcho = hetcho;
        selectedPositions = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return number_chair.size();
    }

    @Override
    public Object getItem(int position) {

        return number_chair.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        CustomView customView = (convertView == null) ? new CustomView(context) : (CustomView) convertView;

        customView.display(number_chair.get(position),selectedPositions.contains(position));

        return customView;
    }
}