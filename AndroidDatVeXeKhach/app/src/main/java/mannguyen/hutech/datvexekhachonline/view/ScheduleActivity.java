package mannguyen.hutech.datvexekhachonline.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;

import mannguyen.hutech.datvexekhachonline.adapter.ScheduleAdapter;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Schedule> schedules = new ArrayList<>();
    private ScheduleAdapter scheduleAdapter;
    private Gson gson;
    private Spinner spinner;
    private TextView txt_sc_start,txt_sc_end;
    public String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        spinner = findViewById(R.id.spinner_name_car);
        txt_sc_start = findViewById(R.id.txt_sc_start);
        txt_sc_end = findViewById(R.id.txt_sc_end);

        recyclerView = findViewById(R.id.rcy_schedule);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ScheduleActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        scheduleAdapter = new ScheduleAdapter(schedules, ScheduleActivity.this);
        recyclerView.setAdapter(scheduleAdapter);

        getNameCars();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinner.getSelectedItem().toString().equals("Tất cả")) {
                    clear();
                    getLichTrinh();

                } else {
                    String nhaxe = spinner.getSelectedItem().toString();
                    clear();
                    setSpinner(nhaxe);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getLichTrinh() {
        Intent intent = getIntent();
        String startplace = intent.getStringExtra("startplace");
        String endplace = intent.getStringExtra("endplace");
        String day = intent.getStringExtra("day");
        id = intent.getStringExtra("id");
        txt_sc_start.setText(startplace);
        txt_sc_end.setText(endplace);

            ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
            Call<List<Schedule>> call = datVeXe.getScheduleforNameCar(startplace, endplace, day);
            call.enqueue(new Callback<List<Schedule>>() {
                @Override
                public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                    if (response.isSuccessful()) {
                        for (Schedule schedule : response.body()) {
                            schedules.add(schedule);
                        }
                        scheduleAdapter.notifyDataSetChanged();
                    } else {
                        Log.e(TAG, response.message());
                    }
                }

                @Override
                public void onFailure(Call<List<Schedule>> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });

    }

    private void getNameCars() {
        final ArrayList<String> listCars = new ArrayList<String>();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<Schedule>> call = datVeXe.getSchedules();
        call.enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    List<Schedule> dataEmployee = response.body();
                    for (int i = 0; i < dataEmployee.size(); i++) {
                        if (!listCars.contains(dataEmployee.get(i).getNhaxe())) {
                            listCars.add(dataEmployee.get(i).getNhaxe());
                        }

                    }
                    listCars.add(0, "Tất cả");
                    ArrayAdapter arrayAdapter = new ArrayAdapter(ScheduleActivity.this, android.R.layout.simple_spinner_item, listCars);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(arrayAdapter);

                } else {
                    Log.e(TAG, response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void setSpinner(String nhaxe) {
        Intent intent = getIntent();
        String startplace = intent.getStringExtra("startplace");
        String endplace = intent.getStringExtra("endplace");
        String day = intent.getStringExtra("day");
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<Schedule>> call = datVeXe.getSchedule(nhaxe, startplace, endplace, day);
        call.enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    for (Schedule schedule : response.body()) {
                        schedules.add(schedule);
                    }
                    scheduleAdapter.notifyDataSetChanged();
                } else {
                    Log.e(TAG, response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public void clear() {
        int size = schedules.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                schedules.remove(0);
            }

            scheduleAdapter.notifyItemRangeRemoved(0, size);
        }
    }
}
