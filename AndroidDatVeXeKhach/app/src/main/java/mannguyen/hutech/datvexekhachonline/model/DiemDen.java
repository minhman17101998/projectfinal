package mannguyen.hutech.datvexekhachonline.model;

import com.google.gson.annotations.SerializedName;

public class DiemDen {
    @SerializedName("diemden")
    private String diemden;

    public String getDiemden() {
        return diemden;
    }
}
