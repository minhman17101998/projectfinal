package mannguyen.hutech.datvexekhachonline.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.adapter.NoiDiAdapter;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.model.DiemDen;
import mannguyen.hutech.datvexekhachonline.model.DiemDi;
import mannguyen.hutech.datvexekhachonline.model.Schedule;
import mannguyen.hutech.datvexekhachonline.view.ChooseSeatActivity;
import mannguyen.hutech.datvexekhachonline.view.DiemDiActivity;
import mannguyen.hutech.datvexekhachonline.view.ScheduleActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.greenrobot.eventbus.EventBus.TAG;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class TicketFragment extends Fragment {
    Calendar cal;
    TextView txt_today;
    Date dateFinish;
    public TicketFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_ticket, container, false);
        // ánh xạ tại đây
        txt_today = root.findViewById(R.id.txt_today);
        //final AutoCompleteTextView edt_start_place = root.findViewById(R.id.edt_start_place);
        final Spinner spn_diem_di = root.findViewById(R.id.spn_diem_di);
        final Spinner spn_diem_den = root.findViewById(R.id.spn_diem_den);
        //final AutoCompleteTextView edt_end_place = root.findViewById(R.id.edt_end_place);
        //final ImageView imageview_change = root.findViewById(R.id.imageview_change);
        final Button btn_search = root.findViewById(R.id.btn_search);
        //final CalendarView calendar_view = root.findViewById(R.id.calendar_view);
        //Sự kiện chỗ này
        setLanguage();
        getDefaultInfor(txt_today);
        //setMindate(calendar_view);
        getPlaceStart(spn_diem_di);
        spn_diem_di.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getPlaceEnd(spn_diem_di,spn_diem_den);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*calendar_view.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String day, month1;
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else
                    day = String.valueOf(dayOfMonth);
                if (month < 10) {
                    month1 = "0" + (month + 1);
                } else {
                    month1 = String.valueOf(month);
                }
                String search_day = day + "/" + month1 + "/" + year;

                txt_today.setText(search_day);
            }
        });*/


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String startplace = (String) spn_diem_di.getSelectedItem();
                final String endplace = (String) spn_diem_den.getSelectedItem();
                final String day = txt_today.getText().toString().trim();

                String yyyy = day.substring(6,10);
                String mm = day.substring(3,5);
                String dd = day.substring(0,2);
                String ngay = yyyy+"-"+mm+"-"+dd;

                if(startplace.equals("Chọn điểm đi") || endplace.equals("Chọn điểm đến") )
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setTitle("Thông báo");
                    b.setMessage("Vui lòng chọn nơi cần đi!");
                    b.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog al = b.create();
                    al.show();

                }else{
                    Intent intent = new Intent(getActivity().getBaseContext(), ScheduleActivity.class);
                    Bundle bundle = new Bundle();
                    // đóng gói kiểu dữ liệu String, Boolean
                    bundle.putString("startplace", startplace);
                    bundle.putString("endplace", endplace);
                    bundle.putString("day", ngay);
                    // đóng gói bundle vào intent
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }
            }
        });

        txt_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialogNgayDi();
            }
        });

        return root;
    }

    public void getDefaultInfor(TextView txt_today) {
        //lấy ngày hiện tại của hệ thống
        cal = Calendar.getInstance();
        SimpleDateFormat dft = null;
        //Định dạng ngày / tháng /năm
        dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = dft.format(cal.getTime());
        //hiển thị lên giao diện
        txt_today.setText(strDate);
    }

    public void setMindate(CalendarView calendarView) {
        Calendar calendar = Calendar.getInstance();
        calendarView.setMinDate(calendar.getTimeInMillis());
    }

    private void setLanguage() {
        Locale locale = new Locale("vi", "Vietnam");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getApplicationContext().getResources().updateConfiguration(config, null);
    }

    private void getPlaceStart(final Spinner spn_diem_di) {
        final ArrayList<String> aaa = new ArrayList<String>();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDi>> call = datVeXe.getNoiDi();
        call.enqueue(new Callback<List<DiemDi>>()
        {
            @Override
            public void onResponse(Call<List<DiemDi>> call, Response<List<DiemDi>> response)
            {
                List<DiemDi> diemDis = response.body();
                for (int i = 0; i <diemDis.size() ; i++)
                {
                    if(!aaa.contains(diemDis.get(i).getDiemdi()))
                    {
                        aaa.add(diemDis.get(i).getDiemdi());
                    }
                }
                Collections.sort(aaa);
                aaa.add(0,"Chọn điểm đi");
                ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, aaa);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spn_diem_di.setAdapter(arrayAdapter);
            }

            @Override
            public void onFailure(Call<List<DiemDi>> call, Throwable t)
            {

            }
        });


    }

    private void getPlaceEnd(final Spinner spn_diem_di,final Spinner spn_diem_den) {
        final ArrayList<String> listplaceend = new ArrayList<String>();
        final String noidi = spn_diem_di.getSelectedItem().toString();
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<DiemDen>> call = datVeXe.getNoiDen(noidi);
        call.enqueue(new Callback<List<DiemDen>>() {
            @Override
            public void onResponse(Call<List<DiemDen>> call, Response<List<DiemDen>> response) {
                List<DiemDen> diemDens = response.body();
                for (int i = 0; i <diemDens.size() ; i++)
                {
                    if(!listplaceend.contains(diemDens.get(i).getDiemden()))
                    {
                        listplaceend.add(diemDens.get(i).getDiemden());
                    }
                }

                if (noidi.equals("Chọn điểm đi"))
                {
                    listplaceend.add(0,"Chọn điểm đến");
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listplaceend);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_diem_den.setAdapter(arrayAdapter);
                }
                else{
                    Collections.sort(listplaceend);

                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, listplaceend);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_diem_den.setAdapter(arrayAdapter);
                }



            }

            @Override
            public void onFailure(Call<List<DiemDen>> call, Throwable t) {

            }
        });
    }

    public void showDatePickerDialogNgayDi()
    {
        DatePickerDialog.OnDateSetListener callback=new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                //Mỗi lần thay đổi ngày tháng năm thì cập nhật lại TextView Date
                String day, month1;
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else
                    day = String.valueOf(dayOfMonth);
                if (monthOfYear < 10) {
                    month1 = "0" + (monthOfYear + 1);
                } else {
                    month1 = String.valueOf(monthOfYear);
                }
                txt_today.setText(
                        (day) +"/"+(month1)+"/"+year);
                /*txtNgayThue.setText(
                        (dayOfMonth) +"/"+(monthOfYear+1)+"/"+year);*/
                //Lưu vết lại biến ngày hoàn thành
                cal.set(year, monthOfYear, dayOfMonth);
                dateFinish=cal.getTime();
            }
        };
        //các lệnh dưới này xử lý ngày giờ trong DatePickerDialog
        //sẽ giống với trên TextView khi mở nó lên
        String s = txt_today.getText()+"";
        String strArrtmp[]=s.split("/");
        int ngay=Integer.parseInt(strArrtmp[0]);
        int thang=Integer.parseInt(strArrtmp[1])-1;
        int nam=Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                getActivity(),
                callback, nam, thang, ngay);
        pic.setTitle("CHỌN NGÀY ĐI");
        pic.show();


    }
}
