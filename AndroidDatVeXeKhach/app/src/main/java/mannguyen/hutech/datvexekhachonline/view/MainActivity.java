package mannguyen.hutech.datvexekhachonline.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import mannguyen.hutech.datvexekhachonline.R;
import mannguyen.hutech.datvexekhachonline.fragment.MyTicketFragment;
import mannguyen.hutech.datvexekhachonline.fragment.SettingFragment;
import mannguyen.hutech.datvexekhachonline.fragment.TicketFragment;
import mannguyen.hutech.datvexekhachonline.fragment.VeFragment;
import mannguyen.hutech.datvexekhachonline.interfaces.ApiInterfaces;
import mannguyen.hutech.datvexekhachonline.interfaces.ServiceGenerator;
import mannguyen.hutech.datvexekhachonline.interfaces.UserID;
import mannguyen.hutech.datvexekhachonline.model.users;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView txtfont1;
    public String tendangnhap,id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtfont1 = findViewById(R.id.txtfont1);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/font.TTF");

        txtfont1.setTypeface(typeface);
        getID();
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new VeFragment(id)).commit();

    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectfragment = null;

                    switch (item.getItemId()){
                        case R.id.nav_ticket:
                            selectfragment = new VeFragment(id);
                            break;
                        case R.id.nav_myticket:
                            selectfragment = new MyTicketFragment();
                            break;
                        case R.id.nav_setting:
                            selectfragment = new SettingFragment(id);
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectfragment).commit();
                    return true;
                }
            };

    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Bạn muốn đóng ứng dụng ?")
                .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent a = new Intent(Intent.ACTION_MAIN);
                        a.addCategory(Intent.CATEGORY_HOME);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);
                    }
                }).setNegativeButton("Trở về", null).show();
    }

    private void getID(){
        Intent intent = getIntent();
        tendangnhap = intent.getStringExtra("username");
        ApiInterfaces datVeXe = ServiceGenerator.createService(ApiInterfaces.class);
        Call<List<users>> call = datVeXe.getId(tendangnhap);
        call.enqueue(new Callback<List<users>>() {
            @Override
            public void onResponse(Call<List<users>> call, Response<List<users>> response) {
                if (response.isSuccessful())
                {
                    List<users> userss = response.body();
                    for (int i = 0 ; i< userss.size();i++)
                    {
                        id = userss.get(i).getId();
                        new UserID(id);
                        Toast.makeText(MainActivity.this, id, Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Log.i("Thong Bao:","Loi");
                }
            }

            @Override
            public void onFailure(Call<List<users>> call, Throwable t) {

            }
        });

    }
}
