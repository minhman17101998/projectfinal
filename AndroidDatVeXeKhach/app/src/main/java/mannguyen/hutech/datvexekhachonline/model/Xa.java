package mannguyen.hutech.datvexekhachonline.model;

public class Xa
{
    private String id;
    private String name;
    private String type;
    private String huyenid;

    public Xa() {
    }

    public Xa(String id, String name, String type, String huyenid) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.huyenid = huyenid;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getHuyenid() {
        return huyenid;
    }
}
