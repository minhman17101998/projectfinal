package mannguyen.hutech.datvexekhachonline.model;

public class Huyen
{
    private String id;
    private String name;
    private String type;
    private String tinhid;

    public Huyen() {

    }

    public Huyen(String id, String name, String type, String tinhid) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.tinhid = tinhid;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getTinhid() {
        return tinhid;
    }
}
